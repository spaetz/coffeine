using Posix;

// This describes the DBus interface that we are intending to use
[DBus (name = "org.freedesktop.ScreenSaver")]
interface ScreenSaver : Object {
    public abstract void Inhibit (string application_name, string reason,
                                  out uint cookie) throws GLib.Error;
}


public static int main(string[] args) {
    // Do nothing if no command was passed into us
    if (args.length == 1) {
	print("USAGE: coffeine <commandline to be executed>\n\n  Learn more "+
	      "about it here: https://gitlab.com/sspaeth/coffeine\n");
        return 0;
    }
    

    var loop = new MainLoop ();

    try {
        ScreenSaver saver_dbus;
        // This is the inhibitor cookie which we could use to uninhibit(),
        // however that is done automatically when we exit.
        uint cookie;

        saver_dbus = Bus.get_proxy_sync (BusType.SESSION,
	                            "org.freedesktop.ScreenSaver",
                                    "/org/freedesktop/ScreenSaver");
	// Inhibit with the called program's name & reason "coffeine"
        saver_dbus.Inhibit( args[1].dup(), "coffeine", out cookie );
    } catch (GLib.Error e) {
        Posix.stderr.printf ("Inhibiting failed: %s\n", e.message);
        return 1;
    }

    // Successfully inhibited screensaver. Now call the program async (so we
    // can pass in/out to STDIN/STDOUT/STDERR while it runs.

    Pid child_pid;  // child_pid needed so we can wait until it exits.
    int retval = 0; // retvalue to passed back to cmdline
    try {
        string[] env = Environ.get ();
        Process.spawn_async_with_pipes (
	    null,        // use current working directory as working dir
            args[1:-1],  // hand it all remaining command line arguments
            env,         // hand it the current environment vars
            SpawnFlags.SEARCH_PATH | SpawnFlags.SEARCH_PATH_FROM_ENVP |
	        SpawnFlags.CHILD_INHERITS_STDIN | SpawnFlags.DO_NOT_REAP_CHILD,
            null,
            out child_pid,
            null,
            null,
            null);	
    } catch (SpawnError e) {
          Posix.stderr.printf("Error spawing program: %s", e.message);
          return 1;
    }

    /* Make sure we close the process using it's pid */
    ChildWatch.add (child_pid, (pid, status) => {
        // This func is executed when the child tells us it has quit
        Process.close_pid (pid);

        // save child process return value
        // (OBS: Limitation: if killed with SIGNAL it will still be 0)
        retval = Process.exit_status(status);
        loop.quit ();            // end the loop.run()
    });


    loop.run ();    // wait until child has exited...
    return retval;
}